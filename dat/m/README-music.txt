The original Meritous game included a set of music files that cannot
be legally redistributed.

Files came mostly from modarchive.org and can be easily
found there using their MD5 sum.

62527c2c36540db199e84c6e138d7c4b  amblight.xm
c006d6e9d1bd376b674162afb9facf24  cave06.s3m
d4f247af052a8580c48fe8d98622d5b7  cavern.xm
1002848d5cbc6c47cc938f42425d7de3  cave.xm
b85aabce1d10158b6ed3b7b99f2695d2  CT_BOSS.MOD
3928053518ddeef0d4a9de4d98d6d6bb  Cv_boss.mod
1a3a43cef4352f57e87304a03158209b  fear2.mod
78195143564fc0f8cbb5543f799a559a  FINALBAT.s3m
8aedc427484fab0cbc178a2744db7049  Fr_boss.mod
e0597bb0db35e9734cf6b6f5d262aa0a  ICEFRONT.S3M
2db6a838796901ade9a44a3931b1c6bc  iller_knarkloader_final.xm
2a063bf70f98dc4c684e6976256c1c0d  rpg_bat1.xm
c976c4002f05585aceaa4f1d212738a8  Wood.s3m

The Credits section of the help file mentioned:

    Music:
     "Ambient Light"       Vogue of Triton
     "Battle of Ragnarok"  Frostbite
     "Dragon Cave"         TICAZ
     cavern.xm             Unknown
     "Caverns Boss"        Alexis Janson
     "Forest Boss"         Alexis Janson
     "Catacombs Boss"      Alexis Janson
     "Fear 2"              Mick Rippon
     "The Final Battle"    Goose/CéDA & iNVASiON
     "Ice Frontier"        Skaven/FC
     "KnarkLoader 1.0"     Rapacious
     "RPG-Battle"          Cyn
     "Metallic Forest"     Joseph Fox

It would be nice to provide replacements for these music files.
